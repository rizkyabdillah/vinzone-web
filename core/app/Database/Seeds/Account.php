<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use App\Models\CrudModel;

class Account extends Seeder
{

    public function run()
    {
        helper('text');

        $DATA_USERS         = array();
        $DATA_PROFILES      = array();
        $DATA_LOCATIONS     = array();
        $DATA_IDGAME        = array();

        for ($i = 0; $i < 100; $i++) {
            $ID_USER = 'USRS-' . strtoupper(random_string('alnum', 15));
            // DATA USERS
            array_push($DATA_USERS, $this->setDataUser($ID_USER));
            // DATA PROFILES
            array_push($DATA_PROFILES, $this->setDataProfile($ID_USER));
            // DATA LOCATIONS
            array_push($DATA_LOCATIONS, $this->setDataLocation($ID_USER));
            // DATA ID_GAME
            for($j = 0; $j < rand(2, 6); $j++) {
                array_push($DATA_IDGAME, $this->setDataIDGame($ID_USER));
            }

        }

        $this->model()->insertDataBatch('PROFILES', $DATA_PROFILES);
        $this->model()->insertDataBatch('LOCATIONS', $DATA_LOCATIONS);
        $this->model()->insertDataBatch('USERS', $DATA_USERS);
        $this->model()->insertDataBatch('ID_GAMES', $DATA_IDGAME);
    }

    private function model()
    {
        return new CrudModel();
    }

    private function setDataUser($idUser)
    {
        return [
            'ID_USER'   => $idUser,
            'USERNAME'  => strtoupper(random_string('alpha', 10). random_string('numeric', 3)),
            'EMAIL'     => 'testmail' . random_string('numeric', 5) . '@gmail.com',
            'PASSWORD'  => password_hash('asd', PASSWORD_BCRYPT),
        ];
    }

    private function setDataProfile($idUser)
    {
        $JK = ['LAKI LAKI', 'PEREMPUAN'];
        return [
            'ID_PROFILE'    => $idUser,
            'NAMA_LENGKAP'  => strtoupper(random_string('alpha', 20)),
            'TELEPON'       => '089' . random_string('numeric', 9),
            'JENIS_KELAMIN' => $JK[rand(0, 1)],
            'FOTO'          => '16920351341692035134_5ff36dcdb10ffa1fe6bf.png',
            'USIA'          => rand(5, 60)
        ];
    }

    private function setDataLocation($idUser)
    {
        return [
            'ID_LOCATION'   => 'L-' . strtoupper(random_string('alpha', 8)) . '00' . random_string('numeric', 8),
            'DESA'          => 'Selogudig Kulon',
            'KECAMATAN'     => 'Pajarakan',
            'KABUPATEN'     => 'Probolinggo',
            'PROVINSI'      => 'Jawa Timur',
            'ID_USER'       => $idUser,
            'LATITUDE'      => '-7.8' . random_string('nozero', 13),
            'LONGITUDE'     => '113.3' . random_string('nozero', 13)
        ];
    }

    private function setDataIDGame($idUser)
    {
        $JENIS_GAME = [
            'MOBILE LEGENDS', 'FREE FIRE', 'PUBG MOBILE', 'GENSHIN IMPACT', 'CLASH OF CLANS'
        ];

        return [
            'ID_USER'       => $idUser,
            'ID_IDGAME'     => 'IDGM-' . strtoupper(random_string('alnum', 15)),
            'ID_GAME'       => random_string('nozero', 7),
            'JENIS_GAME'    => $JENIS_GAME[rand(0, count($JENIS_GAME) - 1)],
            'RANKED'        => 'test',
            'SCREENSHOOT_1' => '16920426571692042657_e786e431097a1ca79210.png',
        ];
    }
}
