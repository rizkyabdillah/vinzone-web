<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiLocation extends BaseApi
{
    function store($idUser)
    {
        $RAW_DATA  = $this->getRaw();

        $CEK_DATA = $this->model->getRowDataArray('LOCATIONS', ['ID_USER' => $idUser]);

        if (is_null($CEK_DATA)) {
            $RAW_DATA['ID_USER'] = $idUser;
            $RAW_DATA['ID_LOCATION'] = 'L-' . strtoupper(random_string('alpha', 8)) . '00' . random_string('numeric', 8);
            $INSERT = $this->model->insertData('LOCATIONS', $RAW_DATA);
            if ($INSERT) {
                return $this->setRespond('Berhasil menambahkan lokasi!!');
            } else {
                return $this->setRespond('Gagal menambahkan lokasi!!', null, 400);
            }
        } else {
            $UPDATE = $this->model->updateData('LOCATIONS', $RAW_DATA, ['ID_LOCATION' => $CEK_DATA['ID_LOCATION']]);
            if ($UPDATE) {
                return $this->setRespond('Berhasil merubah lokasi!!');
            } else {
                return $this->setRespond('Gagal merubah lokasi!!', null, 400);
            }
        }
    }

    function show($idUser)
    {
        $CEK_DATA = $this->model->getRowDataArray('LOCATIONS', ['ID_USER' => $idUser]);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Lokasi anda belum terdaftar!!', null, 404);
        } else {
            $DATA = $this->model->getRowDataArray('LOCATIONS', ['ID_USER' => $idUser]);
            if (!is_null($DATA)) {
                return $this->setRespond('Berhasil mendapatkan lokasi!!', $DATA);
            } else {
                return $this->setRespond('Lokasi tidak ditemukan!!', null, 404);
            }
        }
    }
}
