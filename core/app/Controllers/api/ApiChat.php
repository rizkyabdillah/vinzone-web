<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiChat extends BaseApi
{

    public function showChatRoom()
    {
        $GET_DATA   = $this->getGet();

        $ID_USER                = $GET_DATA['ID_USER'];
        $LAST_CHAT_TIMESTAMP    = $GET_DATA['LAST_CHAT_TIMESTAMP'];

        $QUERY = "SELECT LAST_CHAT_TIMESTAMP FROM CHAT_ROOMS WHERE (ID_USER_SENDER = '" . $ID_USER . "' OR ID_USER_RECEIVER = '" . $ID_USER . "') ORDER BY LAST_CHAT_TIMESTAMP DESC LIMIT 1";
        $CEK_DATA = $this->model->queryRowArray($QUERY);

        if ($CEK_DATA ==  null) {
            return $this->setRespond('Belum ada chat yang terkirim!', null, 404);
        } else {
            if (!hash_equals($LAST_CHAT_TIMESTAMP, $CEK_DATA['LAST_CHAT_TIMESTAMP'])) {
                $QUERY = "SELECT IF( STRCMP( B.ID_USER_SENDER, '" . $ID_USER . "' ) = 0, B.ID_USER_RECEIVER, B.ID_USER_SENDER ) AS ID_USER_RECEIVER, A.NAMA_LENGKAP, CONCAT('" . base_url() . "assets/foto/', A.FOTO) AS FOTO, IFNULL((SELECT CHAT FROM DETAIL_CHATS WHERE ID_CHAT_ROOM = B.ID_CHAT_ROOM ORDER BY CREATED_AT DESC LIMIT 1), '*Pesan gambar') AS LAST_CHAT, IFNULL((SELECT IS_SEEN FROM DETAIL_CHATS WHERE ID_CHAT_ROOM = B.ID_CHAT_ROOM AND ID_USER_SENDER <> '" . $ID_USER . "' ORDER BY CREATED_AT DESC LIMIT 1 ), 1 ) AS IS_SEEN FROM PROFILES AS A INNER JOIN CHAT_ROOMS AS B WHERE ( B.ID_USER_SENDER = '" . $ID_USER . "' OR B.ID_USER_RECEIVER = '" . $ID_USER . "' ) AND A.ID_PROFILE = IF( STRCMP( B.ID_USER_SENDER, '" . $ID_USER . "' ) = 0, B.ID_USER_RECEIVER, B.ID_USER_SENDER ) ORDER BY B.LAST_CHAT_TIMESTAMP DESC";
                $CHAT_DATA = $this->model->queryArray($QUERY);
                $DATA = [
                    'LAST_CHAT_TIMESTAMP'   => $CEK_DATA['LAST_CHAT_TIMESTAMP'],
                    'CHAT_DATA'             => $CHAT_DATA
                ];
                return $this->setRespond('Berhasil mendapatkan data chat!', $DATA);
            } else {
                return $this->setRespond('Tidak ada perubahan chat!!', null, 201);
            }
        }
    }


    public function showChat()
    {
        $GET_DATA   = $this->getGet();

        $ID_USER_SENDER         = $GET_DATA['ID_USER_SENDER'];
        $ID_USER_RECEIVER       = $GET_DATA['ID_USER_RECEIVER'];
        $LAST_CHAT_TIMESTAMP    = $GET_DATA['LAST_CHAT_TIMESTAMP'];

        $QUERY = "SELECT ID_CHAT_ROOM, LAST_CHAT_TIMESTAMP FROM CHAT_ROOMS WHERE (ID_USER_SENDER ='" . $ID_USER_SENDER . "' AND ID_USER_RECEIVER ='" . $ID_USER_RECEIVER . "') OR (ID_USER_SENDER ='" . $ID_USER_RECEIVER . "' AND ID_USER_RECEIVER ='" . $ID_USER_SENDER . "')";
        $CEK_DATA = $this->model->queryRowArray($QUERY);

        $QUERY = "SELECT NAMA_LENGKAP, CONCAT('" . base_url() . "assets/foto/', FOTO) AS FOTO, LAST_ONLINE FROM PROFILES WHERE ID_PROFILE ='" . $ID_USER_RECEIVER . "'";
        $PROFILE_DATA = $this->model->queryRowArray($QUERY);

        if (is_null($PROFILE_DATA['LAST_ONLINE'])) {
            $PROFILE_DATA['STATUS_ONLINE'] = 'Offline';
        } else {
            $DATE_NOW = date_create(date('Y-m-d H:i:s'));
            $DATE_LAST_ONLINE = date_create($PROFILE_DATA['LAST_ONLINE']);

            $INTERVAL = date_diff($DATE_LAST_ONLINE, $DATE_NOW);
            $MINUTES = $INTERVAL->days * 24 * 60;
            $MINUTES += $INTERVAL->h * 60;
            $MINUTES += $INTERVAL->i;

            $PROFILE_DATA['STATUS_ONLINE'] = ($MINUTES > 3) ? 'Offline' : 'Online';
        }

        unset($PROFILE_DATA['LAST_ONLINE']);

        if ($CEK_DATA ==  null) {
            $DATA = [
                'PROFILE'               => $PROFILE_DATA,
                'LAST_CHAT_TIMESTAMP'   => '-',
                'CHAT_DATA'             => []
            ];
            return $this->setRespond('Belum ada chat yang terkirim!', $DATA, 404);
        } else {
            if (!hash_equals($LAST_CHAT_TIMESTAMP, $CEK_DATA['LAST_CHAT_TIMESTAMP'])) {                
                $QUERY = "SELECT ID_CHAT, ID_USER_SENDER, CHAT, CONCAT('" . base_url() . "assets/foto/', GAMBAR) AS GAMBAR, CREATED_AT FROM DETAIL_CHATS WHERE ID_CHAT_ROOM ='" . $CEK_DATA['ID_CHAT_ROOM'] . "' ORDER BY CREATED_AT ";
                $CHAT_DATA = $this->model->queryArray($QUERY);

                $this->model->updateData('DETAIL_CHATS', ['IS_SEEN' => 1], ['ID_CHAT_ROOM' => $CEK_DATA['ID_CHAT_ROOM'], 'ID_USER_SENDER' => $ID_USER_RECEIVER]);

                $DATA = [
                    'PROFILE'               => $PROFILE_DATA,
                    'LAST_CHAT_TIMESTAMP'   => $CEK_DATA['LAST_CHAT_TIMESTAMP'],
                    'CHAT_DATA'             => $CHAT_DATA
                ];
                return $this->setRespond('Berhasil mendapatkan data chat!', $DATA);
            } else {
                return $this->setRespond('Tidak ada perubahan chat!!', null, 201);
            }
        }
    }

    public function send()
    {
        $GET_DATA   = $this->getGet();
        $POST_DATA  = $this->getPost();
        $GAMBAR     = $this->getFile('GAMBAR');

        $ID_USER_SENDER         = $GET_DATA['ID_USER_SENDER'];
        $ID_USER_RECEIVER       = $GET_DATA['ID_USER_RECEIVER'];
        $LAST_CHAT_TIMESTAMP    = date('Y-m-d H:i:s');

        $ID_CHAT_ROOM =  'CR-' . strtoupper(random_string('alnum', 22));

        $QUERY = "SELECT * FROM CHAT_ROOMS WHERE (ID_USER_SENDER ='" . $ID_USER_SENDER . "' AND ID_USER_RECEIVER ='" . $ID_USER_RECEIVER . "') OR (ID_USER_SENDER ='" . $ID_USER_RECEIVER . "' AND ID_USER_RECEIVER ='" . $ID_USER_SENDER . "')";
        $CEK_DATA = $this->model->queryRowArray($QUERY);

        if (is_null($CEK_DATA)) {
            $DATA = [
                'ID_CHAT_ROOM'          => $ID_CHAT_ROOM,
                'ID_USER_SENDER'        => $ID_USER_SENDER,
                'ID_USER_RECEIVER'      => $ID_USER_RECEIVER,
                'LAST_CHAT_TIMESTAMP'   => $LAST_CHAT_TIMESTAMP,
            ];

            $this->model->insertData('CHAT_ROOMS', $DATA);
        } else {
            $ID_CHAT_ROOM = $CEK_DATA['ID_CHAT_ROOM'];
            $this->model->updateData('CHAT_ROOMS', ['LAST_CHAT_TIMESTAMP'   => $LAST_CHAT_TIMESTAMP], ['ID_CHAT_ROOM' => $ID_CHAT_ROOM]);
        }

        $DATA = [
            'ID_CHAT'           => 'CHX-' . strtoupper(random_string('alnum', 26)),
            'ID_CHAT_ROOM'      => $ID_CHAT_ROOM,
            'ID_USER_SENDER'    => $ID_USER_SENDER,
        ];

        if (isset($POST_DATA['CHAT'])) {
            $DATA['CHAT'] = $POST_DATA['CHAT'];
        }

        if ($GAMBAR != null) {
            $DATA['GAMBAR']    = time() . $GAMBAR->getRandomName();
            $GAMBAR->move(FCPATH . 'assets/foto/', $DATA['GAMBAR']);
        }

        $INSERT = $this->model->insertData('DETAIL_CHATS', $DATA);

        $DATA_RECEIVER  = $this->model->getRowDataArray('USERS', ['ID_USER' => $ID_USER_RECEIVER]);
        $DATA_SENDER    = $this->model->getRowDataArray('PROFILES', ['ID_PROFILE' => $ID_USER_SENDER]);

        $PESAN = !isset($POST_DATA['CHAT']) ? 'Pesan berupa gambar!' : $POST_DATA['CHAT'];
        $this->notif->sendNotif($DATA_RECEIVER['TOKEN'], 'Pesan baru dari ' . ucfirst($DATA_SENDER['NAMA_LENGKAP']), $PESAN);

        if ($INSERT) {
            return $this->setRespond('Berhasil mengirimkan chat!!');
        } else {
            return $this->setRespond('Gagal mengirimkan chat!!', null, 400);
        }
    }
}
