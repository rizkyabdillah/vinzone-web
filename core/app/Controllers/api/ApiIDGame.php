<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiIDGame extends BaseApi
{
    function store($idUser)
    {
        $POST_DATA  = $this->getPost();
        $FILE_1     = $this->getFile('SCREENSHOOT_1');
        $FILE_2     = $this->getFile('SCREENSHOOT_2');
        $FILE_3     = $this->getFile('SCREENSHOOT_3');

        $POST_DATA['ID_IDGAME'] =  'IDGM-' . strtoupper(random_string('alnum', 15));
        $POST_DATA['ID_USER'] = $idUser;

        if ($FILE_1 != null) {
            $POST_DATA['SCREENSHOOT_1']    = time() . $FILE_1->getRandomName();
            $FILE_1->move(FCPATH . 'assets/foto/', $POST_DATA['SCREENSHOOT_1']);
        }

        if ($FILE_2 != null) {
            $POST_DATA['SCREENSHOOT_2']    = time() . $FILE_2->getRandomName();
            $FILE_2->move(FCPATH . 'assets/foto/', $POST_DATA['SCREENSHOOT_2']);
        }

        if ($FILE_3 != null) {
            $POST_DATA['SCREENSHOOT_3']    = time() . $FILE_3->getRandomName();
            $FILE_3->move(FCPATH . 'assets/foto/', $POST_DATA['SCREENSHOOT_3']);
        }

        $INSERT = $this->model->insertData('ID_GAMES', $POST_DATA);
        if ($INSERT) {
            return $this->setRespond('Berhasil menambahkan ID Game!!');
        } else {
            return $this->setRespond('Gagal menambahkan lokasi!!', null, 400);
        }
    }

    function show($idUser)
    {
        $QUERY = "SELECT ID_IDGAME, JENIS_GAME, ID_GAME, RANKED, CONCAT('" . base_url() . "assets/foto/', SCREENSHOOT_1) AS SCREENSHOOT_1, CONCAT('" . base_url() . "assets/foto/', SCREENSHOOT_2) AS SCREENSHOOT_2, CONCAT('" . base_url() . "assets/foto/', SCREENSHOOT_3) AS SCREENSHOOT_3 FROM ID_GAMES WHERE ID_USER ='" . $idUser . "'";
        $CEK_DATA = $this->model->queryArray($QUERY);
        if (count($CEK_DATA) < 1) {
            return $this->setRespond('Tidak ada ID Game yang terdaftar!!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan ID Game!!', $CEK_DATA);
        }
    }

    function detail($idIDGame)
    {
        $QUERY = "SELECT ID_IDGAME, JENIS_GAME, ID_GAME, RANKED, CONCAT('" . base_url() . "assets/foto/', SCREENSHOOT_1) AS SCREENSHOOT_1, CONCAT('" . base_url() . "assets/foto/', SCREENSHOOT_2) AS SCREENSHOOT_2, CONCAT('" . base_url() . "assets/foto/', SCREENSHOOT_3) AS SCREENSHOOT_3 FROM ID_GAMES WHERE ID_IDGAME ='" . $idIDGame . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('ID Game tidak ditemukan!!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan ID Game!!', $CEK_DATA);
        }
    }
    
    function update($idIDGame)
    {
        $POST_DATA  = $this->getPost();
        $FILE_1     = $this->getFile('SCREENSHOOT_1');
        $FILE_2     = $this->getFile('SCREENSHOOT_2');
        $FILE_3     = $this->getFile('SCREENSHOOT_3');

        
        if ($FILE_1 != null) {
            $POST_DATA['SCREENSHOOT_1']    = time() . $FILE_1->getRandomName();
            $FILE_1->move(FCPATH . 'assets/foto/', $POST_DATA['SCREENSHOOT_1']);
        }

        if ($FILE_2 != null) {
            $POST_DATA['SCREENSHOOT_2']    = time() . $FILE_2->getRandomName();
            $FILE_2->move(FCPATH . 'assets/foto/', $POST_DATA['SCREENSHOOT_2']);
        }

        if ($FILE_3 != null) {
            $POST_DATA['SCREENSHOOT_3']    = time() . $FILE_3->getRandomName();
            $FILE_3->move(FCPATH . 'assets/foto/', $POST_DATA['SCREENSHOOT_3']);
        }

        $UPDATE = $this->model->updateData('ID_GAMES', $POST_DATA, ['ID_IDGAME' => $idIDGame]);
        if ($UPDATE) {
            return $this->setRespond('Berhasil merubah ID Game!!');
        } else {
            return $this->setRespond('Gagal merubah lokasi!!', null, 400);
        }
    }

    function delete($idIDGame)
    {
        $DELETE = $this->model->deleteData('ID_GAMES', ['ID_IDGAME' => $idIDGame]);
        if ($DELETE) {
            return $this->setRespond('Berhasil menghapus ID Game!!');
        } else {
            return $this->setRespond('Gagal merubah lokasi!!', null, 400);
        }
    }
}
