<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiCariTeman extends BaseApi
{

    function show($idUser)
    {
        $GET_DATA = $this->getGet();

        $DATA = $this->model->getRowDataArray('LOCATIONS', ['ID_USER' => $idUser]);

        if(is_null($DATA)) {
            return $this->setRespond('Lokasi anda masih belum terdaftar, silahkan daftarkan lokasi anda terlebih dahulu!!', null, 400);
        }
        
        $USIA_MIN   = $GET_DATA['USIA_MIN'];
        $USIA_MAX   = $GET_DATA['USIA_MAX'];
        $JARAK      = $GET_DATA['JARAK'];
        $ARR_GAME   = explode('|', $GET_DATA['JENIS_GAME']);
        $LAT        = $DATA['LATITUDE'];
        $LNG        = $DATA['LONGITUDE'];

        $FILTER_GAME = '';
        foreach($ARR_GAME as $i => $val) {
            if($i < 1) {
                $FILTER_GAME .= 'C.JENIS_GAME IN (';
            }

            $FILTER_GAME .= "'" . $val . "'";

            if($i < count($ARR_GAME) - 1) {
                $FILTER_GAME .= ', ';
            }

            if($i == count($ARR_GAME) - 1) {
                $FILTER_GAME .= ')';
            }
        }

        $QUERY = "SELECT A.ID_USER,  A.LATITUDE, A.LONGITUDE, CONCAT('" . base_url() . "assets/foto/', B.FOTO) AS FOTO, ROUND(ST_Distance_Sphere( point('" . $LNG . "', '" . $LAT . "'), point(A.LONGITUDE, A.LATITUDE) ) / 1000, 2) AS DISTANCE FROM LOCATIONS AS A INNER JOIN PROFILES AS B ON(A.ID_USER = B.ID_PROFILE) INNER JOIN ID_GAMES AS C ON (A.ID_USER = C.ID_USER) WHERE (B.USIA BETWEEN '" . $USIA_MIN . "' AND '" . $USIA_MAX . "') AND " . $FILTER_GAME . " AND A.ID_USER <> '" . $idUser . "' GROUP BY A.ID_USER HAVING DISTANCE < '" . $JARAK . "'";
        // return $this->setRespond($QUERY);
        $CEK_DATA = $this->model->queryArray($QUERY);
        if (count($CEK_DATA) < 1) {
            return $this->setRespond('Tidak ada pengguna di sekitar yang ditemukan!!', null, 404);
        } else {
            $DATA = [
                'ME' => [
                    'LATITUDE' => $LAT,
                    'LONGITUDE' => $LNG,
                ],
                'FRIENDS' => $CEK_DATA
            ];
            return $this->setRespond('Berhasil menemukan pengguna di sekitar!!', $DATA);
        }
    }

    public function profile($idUser) {
        $ID_PROFILE = $this->getGet('ID_PROFILE');
        
        $QUERY = "SELECT JENIS_GAME FROM ID_GAMES WHERE JENIS_GAME IN (SELECT JENIS_GAME FROM ID_GAMES WHERE ID_USER ='" . $idUser . "' GROUP BY JENIS_GAME) AND ID_USER ='" . $ID_PROFILE . "' GROUP BY JENIS_GAME";
        $DATA_JENIS_GAME = $this->model->queryArray($QUERY);

        $QUERY = "SELECT CONCAT('" . base_url() . "assets/foto/', SCREENSHOOT_1) AS SCREENSHOOT_1 FROM ID_GAMES WHERE ID_USER ='" . $ID_PROFILE . "'";
        $DATA_SCREENSHOOT = $this->model->queryArray($QUERY);

        $QUERY = "SELECT ID_PROFILE, CONCAT('" . base_url() . "assets/foto/', FOTO) AS FOTO, NAMA_LENGKAP, STATUS FROM PROFILES WHERE ID_PROFILE ='" . $ID_PROFILE . "'";
        $DATA_PROFILE = $this->model->queryRowArray($QUERY);

        $DATA_FRIEND = $this->model->getRowDataArray('FRIENDS', ['ID_USER_ME' => $idUser, 'ID_USER_FRIEND' => $ID_PROFILE]);

        if(is_null($DATA_PROFILE['STATUS'])) {
            $DATA_PROFILE['STATUS'] = 'Belum ada status!';
        }

        $DATA_PROFILE['STATUS_FRIEND'] = is_null($DATA_FRIEND) ? 0 : 1;

        $DATA = [
            'PROFILE'       => $DATA_PROFILE,
            'SCREENSHOOT'   => $DATA_SCREENSHOOT,
            'JENIS_GAME'    => $DATA_JENIS_GAME
        ];

        return $this->setRespond('Berhasil mendapatkan data!', $DATA);
    }

    function showFriend($idUser)
    {
        $QUERY = "SELECT A.ID_PROFILE, A.NAMA_LENGKAP, CONCAT('" . base_url() . "assets/foto/', A.FOTO) AS FOTO, CONCAT(B.KECAMATAN, ', ', B.KABUPATEN) AS ALAMAT FROM PROFILES AS A INNER JOIN LOCATIONS AS B ON(A.ID_PROFILE = B.ID_USER) RIGHT JOIN FRIENDS AS C ON(A.ID_PROFILE = C.ID_USER_FRIEND) WHERE C.ID_USER_ME ='" . $idUser . "' ORDER BY C.CREATED_AT DESC";
        $CEK_DATA = $this->model->queryArray($QUERY);

        if(count($CEK_DATA) < 1) {
            return $this->setRespond('Anda belum memiliki teman, silahkan cari teman anda!', null, 404);
        } else {
            return $this->setRespond('Berhasil menemukan teman!!', $CEK_DATA);
        }
    }

    public function add($idUser) {
        $ID_USER_FRIEND = $this->getGet('ID_USER_FRIEND');

        $CEK_DATA = $this->model->getRowDataArray('FRIENDS', ['ID_USER_ME' => $idUser, 'ID_USER_FRIEND' => $ID_USER_FRIEND]);
        if(is_null($CEK_DATA)) {
            $INSERT = $this->model->insertData('FRIENDS', ['ID_USER_ME' => $idUser, 'ID_USER_FRIEND' => $ID_USER_FRIEND]);
            if($INSERT) {
                return $this->setRespond('Berhasil menambahkan teman!!');
            } else {
                return $this->setRespond('Gagal menambahkan teman!!', null, 400);
            }
        } else {
            $DELETE = $this->model->deleteData('FRIENDS', ['ID_USER_ME' => $idUser, 'ID_USER_FRIEND' => $ID_USER_FRIEND]);
            if($DELETE) {
                return $this->setRespond('Berhasil menghapus teman!!');
            } else {
                return $this->setRespond('Gagal menghapus teman!!', null, 400);
            }
        }
        
    }

}
