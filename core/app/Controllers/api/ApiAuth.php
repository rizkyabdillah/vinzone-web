<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiAuth extends BaseApi
{

    // ---------------------------------------------------------
    // LOGIN FUNCTION
    // ---------------------------------------------------------
    public function login()
    {
        $RAW_DATA  = $this->getRaw();

        $QUERY      = "SELECT A.ID_USER, B.NAMA_LENGKAP, A.PASSWORD, B.STATUS, CONCAT('" . base_url() . "assets/foto/', B.FOTO) AS FOTO FROM USERS AS A INNER JOIN PROFILES AS B ON(A.ID_USER = B.ID_PROFILE) WHERE A.USERNAME ='" . $RAW_DATA['USEREMAIL'] . "' OR A.EMAIL ='" . $RAW_DATA['USEREMAIL'] . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);

        if (is_null($CEK_DATA)) {
            return $this->setRespond('Gagal login', null, 400, [$this->setError('USEREMAIL', 'Username atau email anda tidak terdaftar!')]);
        } else {
            $CEK_PASS = password_verify($RAW_DATA['PASSWORD'], $CEK_DATA['PASSWORD']);
            if ($CEK_PASS) {
                $this->model->updateData('USERS', ['TOKEN' => $RAW_DATA['TOKEN']], ['ID_USER' => $CEK_DATA['ID_USER']]);
                unset($CEK_DATA['PASSWORD']);
                return $this->setRespond('Selamat datang!', $CEK_DATA);
            } else {
                return $this->setRespond('Gagal login', null, 400, [$this->setError('PASSWORD', 'Password anda salah!')]);
            }
        }
    }

    // ---------------------------------------------------------
    // OTP SEND FUNCTION
    // ---------------------------------------------------------
    public function otpSend($email)
    {
        $CEK_DATA = $this->model->getRowDataArray('USERS', ['EMAIL' => $email]);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Email anda tidak terdaftar!', null, 404);
        } else {
            $OTP = random_int(11111, 99999);
            $SEND = $this->email->send(
                $email,
                '<hr><p style="align:text-center;">Kode OTP anda adalah : <h1>' . $OTP . '</h1> </p><hr>'
            );

            if ($SEND) {
                $this->model->updateData('USERS', ['OTP' => $OTP], ['ID_USER' => $CEK_DATA['ID_USER']]);
                return $this->setRespond('OTP berhasil dikirimkan', ['ID_USER' => $CEK_DATA['ID_USER']]);
            } else {
                return $this->setRespond('OTP gagal dikirimkan!', null, 400);
            }
        }
    }

    // ---------------------------------------------------------
    // OTP VERIFICATION FUNCTION
    // ---------------------------------------------------------
    public function otpVerif($idUser)
    {
        $RAW_DATA = $this->getRaw();

        $CEK_DATA = $this->model->getRowDataArray('USERS', ['ID_USER' => $idUser]);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Id user anda tidak terdaftar!', null, 404);
        } else {
            if (is_null($CEK_DATA['OTP'])) {
                return $this->setRespond('OTP tidak tersedia, lakukan request OTP terlebih dahulu!', null, 400);
            } else {
                if (!hash_equals($CEK_DATA['OTP'], $RAW_DATA['OTP'])) {
                    return $this->setRespond('OTP anda salah!', null, 400);
                } else {
                    $this->model->updateData('USERS', ['OTP' => null], ['ID_USER' => $idUser]);
                    return $this->setRespond('Berhasil memverifikasi OTP!');
                }
            }
        }
    }


    // ---------------------------------------------------------
    // RESET PASSWORD FUNCTION
    // ---------------------------------------------------------
    public function passReset($idUser)
    {
        $RAW_DATA = $this->getRaw();

        $CEK_DATA = $this->model->getRowDataArray('USERS', ['ID_USER' => $idUser]);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Id user anda tidak terdaftar!', null, 404);
        } else {
            $UPDATE = $this->model->updateData('USERS', ['PASSWORD' => password_hash($RAW_DATA['PASSWORD'], PASSWORD_BCRYPT)], ['ID_USER' => $idUser]);
            if (!$UPDATE) {
                return $this->setRespond('Gagal mereset password', null, 400);
            } else {
                return $this->setRespond('Berhasil mereset password, silahkan masuk!');
            }
        }
    }


    // ---------------------------------------------------------
    // UPDATE PASSWORD FUNCTION
    // ---------------------------------------------------------
    public function passUpdate($idUser)
    {
        $RAW_DATA = $this->getRaw();

        $CEK_DATA = $this->model->getRowDataArray('USERS', ['ID_USER' => $idUser]);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Id user anda tidak terdaftar!', null, 404);
        } else {
            $CEK_PASS = password_verify($RAW_DATA['PASSWORD_OLD'], $CEK_DATA['PASSWORD']);
            if (!$CEK_PASS) {
                return $this->setRespond('Gagal mengupdate password', null, 400, [$this->setError('PASSWORD_OLD', 'Password lama anda salah!')]);
            }

            $UPDATE = $this->model->updateData('USERS', ['PASSWORD' => password_hash($RAW_DATA['PASSWORD_NEW'], PASSWORD_BCRYPT)], ['ID_USER' => $idUser]);
            if (!$UPDATE) {
                return $this->setRespond('Gagal mengupdate password', null, 400);
            } else {
                return $this->setRespond('Berhasil mengupdate password');
            }
        }
    }
}
