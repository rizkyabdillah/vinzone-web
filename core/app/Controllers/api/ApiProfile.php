<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiProfile extends BaseApi
{

    // ---------------------------------------------------------
    // SHOW DATA FUNCTION
    // ---------------------------------------------------------
    public function show($idUser)
    {
        $QUERY      = "SELECT A.USERNAME, B.NAMA_LENGKAP, B.TELEPON, B.USIA, A.EMAIL, B.JENIS_KELAMIN FROM USERS AS A INNER JOIN PROFILES AS B ON(A.ID_USER = B.ID_PROFILE) WHERE A.ID_USER='" . $idUser . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);

        if (is_null($CEK_DATA)) {
            return $this->setRespond('Data profile tidak ditemukan', null, 404);
        } else {
            return $this->setRespond('Data profile ditemukan!', $CEK_DATA);
        }
    }

    function showFull($idUser) {
        $ID_USER_FRIEND = $this->getGet('ID_USER_FRIEND');


        $QUERY = "SELECT CONCAT('" . base_url() . "assets/foto/', A.FOTO) AS FOTO, A.NAMA_LENGKAP, A.STATUS, A.JENIS_KELAMIN, A.TELEPON, B.EMAIL, A.USIA, CONCAT( 'Desa ', C.DESA, ', Kec. ', C.KECAMATAN, ', Kab/Kota ', C.KABUPATEN, ', ', C.PROVINSI ) AS ALAMAT, IF( ISNULL( ( SELECT CREATED_AT FROM FRIENDS WHERE ID_USER_ME = '" . $idUser . "' AND ID_USER_FRIEND = A.ID_PROFILE LIMIT 1 ) ), '0', '1' ) AS STATUS_FRIEND FROM PROFILES AS A INNER JOIN USERS AS B ON (A.ID_PROFILE = B.ID_USER) INNER JOIN LOCATIONS AS C ON (A.ID_PROFILE = C.ID_USER) WHERE A.ID_PROFILE = '" . $ID_USER_FRIEND . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);
    
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Data profile tidak ditemukan', null, 404);
        } else {
            return $this->setRespond('Data profile ditemukan!', $CEK_DATA);
        }
    }


    // ---------------------------------------------------------
    // UPDATE DATA FUNCTION
    // ---------------------------------------------------------
    public function update($idUser)
    {
        $RAW_DATA  = $this->getRaw();

        $CEK_USERNAME   = $this->model->getRowDataArray('USERS',    ['USERNAME'     =>  $RAW_DATA['USERNAME'], 'ID_USER !=' => $idUser]);
        $CEK_EMAIL      = $this->model->getRowDataArray('USERS',    ['EMAIL'        =>  $RAW_DATA['EMAIL'], 'ID_USER !=' => $idUser]);
        $CEK_TELEPON    = $this->model->getRowDataArray('PROFILES', ['TELEPON'      =>  $RAW_DATA['TELEPON'], 'ID_PROFILE !=' => $idUser]);

        $ERROR = [];
        $isValid = true;

        if (!is_null($CEK_USERNAME)) {
            array_push($ERROR, $this->setError('USERNAME', 'Username tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!is_null($CEK_EMAIL)) {
            array_push($ERROR, $this->setError('EMAIL', 'Email tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!is_null($CEK_TELEPON)) {
            array_push($ERROR, $this->setError('TELEPON', 'Nomor telepon tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!$isValid) {
            return $this->setRespond('Gagal mengubah data!', null, 400, $ERROR);
        }

        $DATA = [
            'USERNAME'      => $RAW_DATA['USERNAME'],
            'EMAIL'         => $RAW_DATA['EMAIL'],
        ];

        $this->model->updateData('USERS', $DATA, ['ID_USER' => $idUser]);


        $DATA = [
            'TELEPON'       => $RAW_DATA['TELEPON'],
            'USIA'          => $RAW_DATA['USIA'],
            'NAMA_LENGKAP'  => $RAW_DATA['NAMA_LENGKAP'],
            'JENIS_KELAMIN' => $RAW_DATA['JENIS_KELAMIN'],
        ];

        $this->model->updateData('PROFILES', $DATA, ['ID_PROFILE' => $idUser]);


        $QUERY      = "SELECT A.USERNAME, B.NAMA_LENGKAP, B.TELEPON, B.USIA, A.EMAIL, B.JENIS_KELAMIN FROM USERS AS A INNER JOIN PROFILES AS B ON(A.ID_USER = B.ID_PROFILE) WHERE A.ID_USER='" . $idUser . "'";

        $DATASET    = $this->model->queryRowArray($QUERY);
        return $this->setRespond('Data profile berhasil diubah!', $DATASET);
    }

    // ---------------------------------------------------------
    // UPDATE FOTO FUNCTION
    // ---------------------------------------------------------
    public function updateFoto($idUser)
    {
        $FILE       = $this->getFile('FOTO');
        
        $NAMA_FOTO  = time() . $FILE->getRandomName();
        $FILE->move(FCPATH . 'assets/foto/', $NAMA_FOTO);

        if ($FILE->hasMoved()) {
            $this->model->updateData('PROFILES', ['FOTO' => $NAMA_FOTO], ['ID_PROFILE' => $idUser]);
            $DATASET = [
                'FOTO' => base_url() . "assets/foto/" . $NAMA_FOTO
            ];
            return $this->setRespond('Berhasil memperbaruhi foto profile', $DATASET);
        } else {
            return $this->setRespond('Gagal memperbaruhi foto profile', null, 400);
        }
    }

    // ---------------------------------------------------------
    // UPDATE FOTO FUNCTION
    // ---------------------------------------------------------
    public function updateStatus($idUser)
    {
        $RAW_DATA       = $this->getRaw();

        $UPDATE = $this->model->updateData('PROFILES', $RAW_DATA, ['ID_PROFILE' => $idUser]);
        if ($UPDATE) {
            return $this->setRespond('Berhasil memperbaruhi status', $RAW_DATA);
        } else {
            return $this->setRespond('Gagal memperbaruhi status', null, 400);
        }
    }
}
