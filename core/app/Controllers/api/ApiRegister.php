<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiRegister extends BaseApi
{

    public function register()
    {
        $POST_DATA  = $this->getPost();
        $FILE       = $this->getFile('FOTO');

        $NAMA_FOTO  = time() . $FILE->getRandomName();
        $ID_USER    = 'USRS-' . strtoupper(random_string('alnum', 15));

        $CEK_USERNAME   = $this->model->getRowDataArray('USERS',    ['USERNAME'     =>  $POST_DATA['USERNAME']]);
        $CEK_EMAIL      = $this->model->getRowDataArray('USERS',    ['EMAIL'        =>  $POST_DATA['EMAIL']]);
        $CEK_TELEPON    = $this->model->getRowDataArray('PROFILES', ['TELEPON'      =>  $POST_DATA['TELEPON']]);

        $ERROR = [];
        $isValid = true;

        if (!is_null($CEK_USERNAME)) {
            array_push($ERROR, $this->setError('USERNAME', 'Username tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!is_null($CEK_EMAIL)) {
            array_push($ERROR, $this->setError('EMAIL', 'Email tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!is_null($CEK_TELEPON)) {
            array_push($ERROR, $this->setError('TELEPON', 'Nomor telepon tersebut sudah terdaftar'));
            $isValid = false;
        }
        
        if(!$isValid) {
            return $this->setRespond('Gagal registrasi!', null, 400, $ERROR);
        }

        $FILE->move(FCPATH . 'assets/foto/', $NAMA_FOTO);

        $DATA = [
            'ID_USER'       => $ID_USER,
            'USERNAME'      => $POST_DATA['USERNAME'],
            'EMAIL'         => $POST_DATA['EMAIL'],
            'PASSWORD'      => password_hash($POST_DATA['PASSWORD'], PASSWORD_BCRYPT),
            'TOKEN'         => $POST_DATA['TOKEN'],
        ];

        $this->model->insertData('USERS', $DATA);


        $DATA = [
            'ID_PROFILE'    => $ID_USER,
            'TELEPON'       => $POST_DATA['TELEPON'],
            'USIA'          => $POST_DATA['USIA'],
            'NAMA_LENGKAP'  => $POST_DATA['NAMA_LENGKAP'],
            'JENIS_KELAMIN' => $POST_DATA['JENIS_KELAMIN'],
            'FOTO'          => $NAMA_FOTO,
        ];

        $this->model->insertData('PROFILES', $DATA);

        $QUERY      = "SELECT A.ID_USER, B.NAMA_LENGKAP, B.STATUS, CONCAT('" . base_url() . "assets/foto/', B.FOTO) AS FOTO FROM USERS AS A INNER JOIN PROFILES AS B ON(A.ID_USER = B.ID_PROFILE) WHERE A.ID_USER ='" . $ID_USER . "'";
        $DATASET    = $this->model->queryRowArray($QUERY);
        return $this->setRespond('Registrasi berhasil, selamat datang!', $DATASET);

    }
}
