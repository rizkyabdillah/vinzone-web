<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiEvent extends BaseApi
{

    public function show($idEvent = null)
    { {
            if (!is_null($idEvent)) {
                $QUERY = "SELECT ID_EVENT, NAMA_EVENT, TANGGAL, JAM_MULAI, JAM_BERAKHIR, JENIS_GAME, TEMPAT_ACARA, PENYEDIA, TELP_DIHUBUNGI, PERATURAN, CONCAT('" . base_url() . "assets/foto/', GAMBAR) AS GAMBAR, CREATED_AT FROM EVENTS WHERE ID_EVENT ='" . $idEvent . "'";
                $DATASET = $this->model->queryRowArray($QUERY);
                if (is_null($DATASET)) {
                    return $this->setRespond('Data event tidak ditemukan!', null, 404);
                } else {
                    return $this->setRespond('Data event ditemukan!', $DATASET);
                }
            }
        } {
            /* = Status : (0) -> AKAN DATANG, (1) -> BERLANGSUNG, (2) -> SELESAI */
            $GET_DATA   = $this->getGet();

            $FILTER = "";
            $SORT   = " ORDER BY TANGGAL DESC";

            if (count($GET_DATA) > 0) {
                $FILTER = " WHERE ";

                if (isset($GET_DATA['status'])) {
                    switch ($GET_DATA['status']) {
                        case 0:
                            $FILTER .= "((TANGGAL > CURDATE()) OR (TANGGAL = CURDATE() AND JAM_MULAI > CURTIME()))";
                            $SORT = ' ORDER BY TANGGAL ASC, JAM_MULAI ASC';
                            break;
                        case 1:
                            $FILTER .= "(TANGGAL = CURDATE() AND JAM_MULAI < CURTIME() AND JAM_BERAKHIR > CURTIME())";
                            $SORT = ' ORDER BY JAM_MULAI ASC';
                            break;
                        case 2:
                            $FILTER .= "((TANGGAL < CURDATE()) OR (TANGGAL = CURDATE() AND JAM_BERAKHIR < CURTIME()))";
                            $SORT = ' ORDER BY TANGGAL DESC, JAM_MULAI DESC';
                            break;
                    }
                }

                if (isset($GET_DATA['status']) && isset($GET_DATA['jenisGame'])) {
                    $FILTER .= " AND ";
                }

                if (isset($GET_DATA['jenisGame'])) {
                    $FILTER .= "JENIS_GAME ='" . $GET_DATA['jenisGame'] . "'";
                }
                
            }

            $QUERY = "SELECT ID_EVENT, NAMA_EVENT, TANGGAL, JAM_MULAI, JAM_BERAKHIR, JENIS_GAME, TEMPAT_ACARA, PENYEDIA, TELP_DIHUBUNGI, PERATURAN, CONCAT('" . base_url() . "assets/foto/', GAMBAR) AS GAMBAR, CREATED_AT FROM EVENTS" . $FILTER . $SORT;
            // return dd($QUERY);
            $DATASET = $this->model->queryArray($QUERY);

            if (count($DATASET) < 1) {
                return $this->setRespond('Data event tidak ditemukan! ', null, 404);
            } else {
                return $this->setRespond('Data event ditemukan!', $DATASET);
            }
        }
    }
}
