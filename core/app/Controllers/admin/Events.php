<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;

class Events extends BaseController
{

    private function arrayDefault()
    {
        return [
            'titlePage'         => 'VINZONE - Admin Events',
            'sectionTitle'      => 'Data Events',
            'linkBreadCrumb'    => url_to('events-index'),
            'isBack'            => false,
            'breadCrumb'        => [
                'Master Data', 'Kelola Events', ''
            ],
        ];
    }

    public function index()
    {
        $DATASET = $this->model->getAllDataArray('EVENTS');
        $data = [
            'data'  => $DATASET
        ];
        return view('events', array_merge($this->arrayDefault(), $data));
    }

    public function indexAdd()
    {
        $data = [
            'isBack' => true,
        ];
        return view('events_add', array_merge($this->arrayDefault(), $data));
    }

    public function insert()
    {
        $POST_DATA  = $this->getPost();
        $FILE       = $this->getFile('GAMBAR');
        unset($POST_DATA['csrf_test_name']);

        $POST_DATA['GAMBAR']    = time() . $FILE->getRandomName();
        $POST_DATA['ID_EVENT']   = 'EVNT-' . strtoupper(random_string('alnum', 15));
        $FILE->move(FCPATH . 'assets/foto/', $POST_DATA['GAMBAR']);

        $this->model->insertData('EVENTS', $POST_DATA);
        session()->setFlashData('pesan', 'Data berhasil disimpan!');
        return redirect()->to(url_to('events-index'));
    }

    public function indexEdit($idEvent)
    {
        $DATASET = $this->model->getRowDataArray('EVENTS', ['ID_EVENT' => $idEvent]);
        $data = [
            'isBack'    => true,
            'data'      => $DATASET
        ];
        return view('events_edit', array_merge($this->arrayDefault(), $data));
    }

    public function update($idEvent)
    {
        $POST_DATA  = $this->getPost();
        $FILE       = $this->getFile('GAMBAR');
        unset($POST_DATA['csrf_test_name']);
        unset($POST_DATA['_method']);
        // return dd($FILE);

        if (!empty($FILE->getName())) {
            $POST_DATA['GAMBAR']    = time() . $FILE->getRandomName();
            $FILE->move(FCPATH . 'assets/foto/', $POST_DATA['GAMBAR']);
        }

        $this->model->updateData('EVENTS', $POST_DATA, ['ID_EVENT' => $idEvent]);
        session()->setFlashData('pesan', 'Data berhasil diubah!');
        return redirect()->to(url_to('events-index'));
    }

    public function delete($idEvent)
    {
        $this->model->deleteData('EVENTS', ['ID_EVENT' => $idEvent]);
        session()->setFlashData('pesan', 'Data berhasil dihapus!');
        return redirect()->to(url_to('events-index'));
    }
}
