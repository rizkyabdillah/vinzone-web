<?php

namespace App\Controllers\admin;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    private function arrayDefault()
    {
        return [
            'titlePage'         => 'VINZONE - Admin Dashboard',
            'sectionTitle'      => 'Dashboard',
            'linkBreadCrumb'    => url_to('dash-index'),
            'isBack'            => false,
            'breadCrumb'        => [
                'Dashboard', 'Admin Dashboard', ''
            ],
        ];
    }

    public function index()
    {
        $COUNT_PENGGUNA     = $this->model->getDataCount('USERS', 'ID_USER');
        $COUNT_LOKASI       = $this->model->getDataCount('LOCATIONS', 'ID_LOCATION');
        $COUNT_ID_GAME      = $this->model->getDataCount('ID_GAMES', 'ID_IDGAME');
        $COUNT_DETAIL_CHAT  = $this->model->getDataCount('DETAIL_CHATS', 'ID_CHAT');

        $data = [
            'countPengguna' => $COUNT_PENGGUNA['ID_USER'],
            'countLokasi'   => $COUNT_LOKASI['ID_LOCATION'],
            'countIDGame'   => $COUNT_ID_GAME['ID_IDGAME'],
            'countChat'     => $COUNT_DETAIL_CHAT['ID_CHAT']
        ];

        return view('dashboard', array_merge($this->arrayDefault(), $data));
    }
}
