<?php

namespace App\Controllers\admin;
use App\Controllers\BaseController;

class Auth extends BaseController
{

    public function index()
    {
        return view('auth');
    }

    public function auth()
    {
        $DATA = $this->getPost();
        if(!hash_equals($DATA['PASSWORD'], 'ADMIN')) {
            session()->setFlashData('pesan', 'Username atau password anda salah!');
            return redirect()->back();
        }

        session()->set(['IS_LOGIN_ADMIN' => 1]);
        return redirect()->to(route_to('dash-index'));
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to(route_to('auth-index'));
    }
}
