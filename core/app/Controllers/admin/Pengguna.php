<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;

class Pengguna extends BaseController
{

    private function arrayDefault()
    {
        return [
            'titlePage'         => 'VINZONE - Admin Pengguna',
            'sectionTitle'      => 'Data Pengguna',
            'linkBreadCrumb'    => url_to('pengguna-index'),
            'isBack'            => false,
            'breadCrumb'        => [
                'Master Data', 'Kelola Pengguna', ''
            ],
        ];
    }

    public function index()
    {
        $QUERY = "SELECT B.ID_USER, A.FOTO, A.NAMA_LENGKAP, A.TELEPON, A.JENIS_KELAMIN, A.STATUS, A.USIA, B.USERNAME, B.EMAIL FROM PROFILES AS A INNER JOIN USERS AS B ON(A.ID_PROFILE = B.ID_USER) ORDER BY B.CREATED_AT DESC";
        $DATASET = $this->model->queryArray($QUERY);

        $data = [
            'data'  => $DATASET
        ];
        return view('pengguna', array_merge($this->arrayDefault(), $data));
    }
}
