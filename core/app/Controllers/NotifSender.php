<?php 
namespace App\Controllers;

class NotifSender {

    const URI = "https://fcm.googleapis.com/fcm/send";
    const CONTENT_TYPE = "application/json";
    const SERVER_KEY = "AAAAluPBBYs:APA91bGMPfNfdeiNda7kd9Tk2yFWmxIBA7HJoqA73zU1TZ5GpmZbCr9hyuzhMtk4gAbDTJr4hjFIRSjrfHTM1XGBopicxr-0nABwz6-nq_hqvOWVGltMPvP1Z7B6RkVTa0jPndNgNJVy";
    const AUTHORIZATION = "key=" . self::SERVER_KEY;

    private function getHeader() {
        return [
            'Authorization: ' . self::AUTHORIZATION,
            'Content-Type: ' . self::CONTENT_TYPE
        ];
    }

    private function getData($ACCESS_TOKEN, $TITLE, $BODY) {
        return [
            "to" => $ACCESS_TOKEN,
            "notification" => [
                "title"=> $TITLE,
                "body" => $BODY
            ]
               
        ];
    }

    private function getMultipleData($MULTIPLE_ACCESS_TOKEN, $TITLE, $BODY) {
        return [
            "registration_ids" => $MULTIPLE_ACCESS_TOKEN,
            "notification" => [
                "title"=> $TITLE,
                "body" => $BODY
            ]
               
        ];
    }

    public function sendNotif($ACCESS_TOKEN, $TITLE, $BODY) {
        $CURL = curl_init(self::URI);

        curl_setopt($CURL, CURLOPT_POST, true);
        curl_setopt($CURL, CURLOPT_HTTPHEADER, $this->getHeader());
        curl_setopt($CURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($CURL, CURLOPT_POSTFIELDS, json_encode($this->getData($ACCESS_TOKEN, $TITLE, $BODY)));

        return curl_exec($CURL);
    }

    public function sendMultipleNotif($MULTIPLE_ACCESS_TOKEN, $TITLE, $BODY) {    
        $CURL = curl_init(self::URI);

        curl_setopt($CURL, CURLOPT_POST, true);
        curl_setopt($CURL, CURLOPT_HTTPHEADER, $this->getHeader());
        curl_setopt($CURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($CURL, CURLOPT_POSTFIELDS, json_encode($this->getMultipleData($MULTIPLE_ACCESS_TOKEN, $TITLE, $BODY)));

        return curl_exec($CURL);
    }

}
