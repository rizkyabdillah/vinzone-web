<?= $this->extend('part/Master') ?>

<?= $this->section('Content') ?>
<div class="card card-primary">
    <div class="card-header">
        <i class="fas fa-home"></i>&nbsp;&nbsp;&nbsp;
        <h4>Kelola Data Events</h4>
        <div class="card-header-action float-right">
            <a href="<?= url_to('events-add-index') ?>" class="btn btn-primary">
                <i class="fas fa-plus"></i>&nbsp;&nbsp; Tambah Data&nbsp;
            </a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped" id="table-1">
                <thead>
                    <tr>
                        <th class="text-center"> No </th>
                        <th>Nama Event</th>
                        <th>Tanggal</th>
                        <th>Waktu</th>
                        <th>Jenis Game</th>
                        <th>Penyedia</th>
                        <th>Tempat Acara</th>
                        <th class="text-center">Peraturan</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($data as $val) :
                    ?>
                        <tr>
                            <td class="text-center"><?= $i++ ?></td>
                            <td><?= $val['NAMA_EVENT'] ?></td>
                            <td><?= $val['TANGGAL'] ?></td>
                            <td><?= $val['JAM_MULAI'] . ' - ' . $val['JAM_BERAKHIR'] ?></td>
                            <td><?= $val['JENIS_GAME'] ?></td>
                            <td><?= $val['PENYEDIA'] ?></td>
                            <td><?= $val['TEMPAT_ACARA'] ?></td>
                            <td class="text-center">
                                <a href="#" class="btn btn-primary btn-action btn-peraturan" data-id="<?= $val['ID_EVENT'] ?>" data-toggle="tooltip" data-original-title="Lihat Peraturan">
                                    <i class="fas fa-eye"></i> Peraturan
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="<?= url_to('events-edit-index', $val['ID_EVENT']); ?>" class="btn btn-primary btn-action" data-toggle="tooltip" data-original-title="Ubah">
                                    <i class="fas fa-pencil-alt"></i> Edit
                                </a>
                                <a data-id="<?= $val['ID_EVENT']; ?>" class="btn btn-danger btn-action ml-1 swal-confirm" data-toggle="tooltip" data-original-title="Hapus">
                                    <form action="<?= url_to('events-delete', $val['ID_EVENT']); ?>" method="POST" id="hapus<?= $val['ID_EVENT']; ?>" class="">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="DELETE" />
                                    </form>
                                    <i class="fas fa-trash"></i> Hapus
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer bg-whitesmoke">
        <i>Copyright By <?= getenv('APP_NAME') ?></i>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('Modal') ?>
<div class="modal fade" id="modalViewCenter" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle">Peraturan Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <hr>
                <p id="peraturan_">

                </p>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('CSSModules') ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<?= $this->endSection() ?>

<?= $this->section('JSModules') ?>
<script src="<?= base_url(); ?>assets/modules/datatables/datatables.min.js"></script>
<script src="<?= base_url(); ?>assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url(); ?>assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
<?= $this->endSection() ?>

<?= $this->section('JSSpecific') ?>
<script src="<?= base_url(); ?>assets/js/page/modules-datatables.js"></script>
<?= $this->endSection() ?>

<?= $this->section('JSTemplate') ?>
<script>
    $(document).on("click", ".swal-confirm", function(e) {
        const id = $(this).data('id');
        swal({
                title: 'Apakah anda yakin?',
                text: 'Disaat anda menghapus, data yang terhapus tidak dapat dikembalikan lagi!',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $('#hapus'.concat(id)).submit();
                }
            });
    });

    $(document).on("click", ".btn-peraturan", function(e) {
        const id = $(this).data('id');
        const url = "<?= base_url() ?>/api/events/" + id;
        $.get(url, function(data, status) {
            console.log(status);
            if (status === "success") {
                console.log(data);
                $('#peraturan_').html("" + data.data.PERATURAN);
                $('#modalViewCenter').modal('show');
            }
        });
    });

    <?php if (session()->getFlashData('pesan')) : ?>
        swal('Sukses', '<?= session()->getFlashData('pesan'); ?>', 'success', {
            buttons: false,
            timer: 1200,
        });
    <?php endif ?>
</script>
<?= $this->endSection() ?>