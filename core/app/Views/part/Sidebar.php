<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?= url_to('dash-index') ?>"><?= getenv('APP_NAME') ?></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?= url_to('dash-index') ?>">VZ</a>
        </div>
        <ul class="sidebar-menu">
            <!-- ==== -->
            <li class="menu-header">Dashboard</li>

            <li id="dashboard">
                <a class="nav-link" href="<?= url_to('dash-index') ?>">
                    <i class="fas fa-chart-line"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- ==== -->
            <li class="menu-header">Master Data</li>

            <li id="kelola-pengguna">
                <a class="nav-link" href="<?= url_to('pengguna-index') ?>">
                    <i class="fas fa-users"></i>
                    <span>Kelola Pengguna</span>
                </a>
            </li>

            <li id="kelola-events">
                <a class="nav-link" href="<?= url_to('events-index') ?>">
                    <i class="fas fa-trophy"></i>
                    <span>Kelola Events</span>
                </a>
            </li>
        </ul>
    </aside>
</div>