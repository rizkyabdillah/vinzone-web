<?= $this->extend('part/Master') ?>

<?= $this->section('Content') ?>
<div class="card card-primary">
    <div class="card-header">
        <h4>Tambah Data Events</h4>
    </div>
    <div class="card-body">
        <form method="POST" action="<?= url_to('events-update', $data['ID_EVENT']); ?>" class="needs-validation form-simpan" novalidate="" enctype="multipart/form-data">
            <?= csrf_field(); ?>
            <input type="hidden" name="_method" value="PUT" />

            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="nama-event">Nama Event</label>
                    <input type="text" value="<?= $data['NAMA_EVENT'] ?>" class="form-control" id="nama-event" name="NAMA_EVENT" placeholder="Masukkan Nama Event" onkeyup="this.value = this.value.toUpperCase()" required>
                    <div class="invalid-feedback" id="inv-nama-event"></div>
                </div>
                <div class="form-group col-md-3">
                    <label for="tanggal">Tanggal</label>
                    <input type="date" value="<?= $data['TANGGAL'] ?>" class="form-control disabled" id="tanggal" name="TANGGAL" required>
                    <div class="invalid-feedback" id="inv-tanggal"></div>
                </div>
                <div class="form-group col-md-2">
                    <label for="jam-mulai">Jam Mulai</label>
                    <input type="time" value="<?= $data['JAM_MULAI'] ?>" class="form-control disabled" id="jam-mulai" name="JAM_MULAI" required>
                    <div class="invalid-feedback" id="inv-jam-mulai"></div>
                </div>
                <div class="form-group col-md-2">
                    <label for="jam-berakhir">Jam Berakhir</label>
                    <input type="time" value="<?= $data['JAM_BERAKHIR'] ?>" class="form-control disabled" id="jam-berakhir" name="JAM_BERAKHIR" required>
                    <div class="invalid-feedback" id="inv-jam-berakhir"></div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="jenis-game">Jenis Game</label>
                    <?php
                    $option = [
                        'MOBILE LEGEND'     => 'Mobile Legend',
                        'PUBG MOBILE'       => 'PUBG Mobile',
                        'FREE FIRE'         => 'Free Fire',
                        'GENSHIN IMPACT'    => 'Genshin Impact',
                        'CLASH OF CLANS'    => 'Clash of Clans',
                    ];
                    $js = [
                        'class'         => 'form-control selectric',
                        'id'            => 'jenis-game'
                    ];
                    echo form_dropdown('JENIS_GAME', $option,  $data['JENIS_GAME'], $js);
                    ?>
                    <div class="invalid-feedback" id="inv-jenis-game"></div>
                </div>
                <div class="form-group col-md-4">
                    <label for="penyedia-event">Penyedia Event</label>
                    <input type="text" value="<?= $data['PENYEDIA'] ?>" class="form-control" id="penyedia-event" name="PENYEDIA" placeholder="Masukkan Nama Penyedia Event" required>
                    <div class="invalid-feedback" id="inv-penyedia-event"></div>
                </div>
                <div class="form-group col-md-4">
                    <label for="telp-dihubungi">Telpon yg Dapat Dihubungi</label>
                    <input type="text" value="<?= $data['TELP_DIHUBUNGI'] ?>" class="form-control" id="telp-dihubungi" name="TELP_DIHUBUNGI" placeholder="Masukkan Telp yg Dapat dihubungi" required>
                    <div class="invalid-feedback" id="inv-telp-dihubungi"></div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="tempat-acara">Tempat Acara Event</label>
                    <textarea class="form-control" id="tempat-acara" name="TEMPAT_ACARA" required><?= $data['TEMPAT_ACARA'] ?></textarea>
                    <div class="invalid-feedback" id="inv-tempat-acara"></div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="peraturan">Peraturan Bermain</label>
                    <textarea class="summernote" id="peraturan" name="PERATURAN" required><?= $data['PERATURAN'] ?></textarea>
                    <div class="invalid-feedback" id="inv-peraturan"></div>
                </div>
            </div>

            <hr>

            <div class="form-row">
                <div class="form-group col-md-7">
                    <label for="alamat">Banner / Gambar Event</label>
                    <img src="<?= base_url() ?>/assets/foto/<?= $data['GAMBAR'] ?>" class="img-thumbnail img-preview" />
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="gambar">Pilih Gambar</label>
                    <input onchange="previewImg()" id="gambar" type="file" name="GAMBAR" class="form-control foto" placeholder="Upload Image" accept=".png, .jpg, .webp, .jpeg">
                    <div class="invalid-feedback" id="inv-gambar"></div>
                </div>
            </div>

        </form>
        <div class="card-footer text-right">
            <button type="button" class="btn btn-primary btn-lg btn-simpan">
                <i class="fas fa-save"></i> Simpan
            </button>
        </div>

    </div>
    <div class="card-footer bg-whitesmoke">
        <i>Copyright By <?= getenv('APP_NAME') ?></i>
    </div>
</div>
<?= $this->endSection() ?>


<?= $this->section('CSSModules') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/modules/jquery-selectric/selectric.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/modules/summernote/summernote-bs4.css">
<?= $this->endSection() ?>

<?= $this->section('JSModules') ?>
<script src="<?= base_url() ?>/assets/modules/jquery-selectric/jquery.selectric.min.js"></script>
<script src="<?= base_url() ?>/assets/modules/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?= base_url() ?>/assets/modules/summernote/summernote-bs4.js"></script>
<?= $this->endSection() ?>

<?= $this->section('JSSpecific') ?>
<?= $this->endSection() ?>

<?= $this->section('JSTemplate') ?>
<script text="text/javascript">
    $(document).on("click", ".btn-simpan", function(e) {
        const name = ['nama-event', 'tanggal', 'jam-mulai', 'jam-berakhir', 'penyedia-event', 'telp-dihubungi', 'tempat-acara'];
        var isValid = true;

        name.forEach(function(item, index) {
            var value = $('#' + item).val();
            if (value === "") {
                isValid = false;
                $('#' + item).addClass('is-invalid');
                $('#inv-' + item).text(item.replace('-', ' ').charAt(0).toUpperCase() + item.replace('-', ' ').slice(1) + ' tidak boleh kosong!');
            }
        });

        if ($('#peraturan').summernote('isEmpty')) {
            $('#peraturan').addClass('is-invalid');
            $('#inv-peraturan').text('Peraturan event boleh kosong');
            isValid = false;
        }

        if (isValid) {
            $('.form-simpan').submit();
        }
    });

    $(document).on("keydown", ".disabled", function(e) {
        e.preventDefault();
        return false;
    });
</script>
<?= $this->endSection() ?>