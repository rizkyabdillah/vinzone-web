<?= $this->extend('part/Master') ?>

<?= $this->section('Content') ?>
<div class="card card-primary">
    <div class="card-header">
        <i class="fas fa-home"></i>&nbsp;&nbsp;&nbsp;
        <h4>Kelola Data Pengguna</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped" id="table-5">
                <thead>
                    <tr>
                        <th class="text-center"> No </th>
                        <th>Nama Lengkap</th>
                        <th>Telepon</th>
                        <th>Jenis Kelamin</th>
                        <th>Usia</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($data as $val) :
                    ?>
                        <tr>
                            <td class="text-center"><?= $i++ ?></td>
                            <td><?= $val['NAMA_LENGKAP'] ?></td>
                            <td><?= $val['TELEPON'] ?></td>
                            <td><?= $val['JENIS_KELAMIN'] ?></td>
                            <td><?= $val['USIA'] . ' Tahun' ?></td>
                            <td><?= $val['USERNAME'] ?></td>
                            <td><?= $val['EMAIL'] ?></td>
                            <td><?= is_null($val['STATUS']) ? '<i>*Tidak ada status</i>' :  $val['STATUS'] ?></td>
                            <td class="text-center">
                                <a href="#" class="btn btn-primary btn-action btn-view-photo" data-photo="<?= $val['FOTO'] ?>" data-toggle="tooltip" data-original-title="Lihat Foto">
                                    <i class="fas fa-images"></i> Lihat Foto
                                </a> |
                                <a href="#" class="btn btn-success btn-action btn-view-id-game" data-id="<?= $val['ID_USER'] ?>" data-toggle="tooltip" data-original-title="Lihat ID Game">
                                    <i class="fas fa-dice"></i> Lihat ID Game
                                </a> |
                                <a href="#" class="btn btn-info btn-action btn-view-location" data-id="<?= $val['ID_USER'] ?>" data-toggle="tooltip" data-original-title="Lihat Lokasi">
                                    <i class="fas fa-map-pin"></i> Lihat Lokasi
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer bg-whitesmoke">
        <i>Copyright By <?= getenv('APP_NAME') ?></i>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('Modal') ?>
<div class="modal fade" id="modalViewPhoto" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle">Foto Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <hr>
                <div class="form-group col-md-12">
                    <img src="#" id="viewPhoto" width="400" />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalViewIDGame" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle">List ID Game</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <hr>
                <table class="table table-striped" id="table-3">
                    <thead>
                        <tr>
                            <th class="text-center"> No </th>
                            <th>Jenis Game</th>
                            <th>ID Game</th>
                            <th>Ranked</th>
                            <th>Screenshoot 1</th>
                            <th>Screenshoot 2</th>
                            <th>Screenshoot 3</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-modal">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('CSSModules') ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<?= $this->endSection() ?>

<?= $this->section('JSModules') ?>
<script src="<?= base_url(); ?>assets/modules/datatables/datatables.min.js"></script>
<script src="<?= base_url(); ?>assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url(); ?>assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
<?= $this->endSection() ?>

<?= $this->section('JSSpecific') ?>
<script src="<?= base_url(); ?>assets/js/page/modules-datatables.js"></script>
<?= $this->endSection() ?>

<?= $this->section('JSTemplate') ?>
<script>
    $(document).on("click", ".btn-view-photo", function(e) {
        const photo = $(this).data('photo');
        $('#viewPhoto').attr("src", '<?= base_url() ?>assets/foto/' + photo);
        $('#modalViewPhoto').modal('show');
    });

    $(document).on("click", ".btn-view-id-game", function(e) {
        const id = $(this).data('id');
        const url = "<?= base_url() ?>/api/idgames/" + id;
        $.get(url, function(data, status) {
            console.log(status);
            if (status === "success") {
                console.log(data);
                var tbody = "";

                for (var i = 0; i < data.data.length; i++) {
                    console.log(data.data[i].RANKED);
                    tbody += '<tr>' +
                        '<td class="text-center">' + (i + 1) + '</td>' +
                        '<td>' + data.data[i].JENIS_GAME + '</td>' +
                        '<td>' + data.data[i].ID_IDGAME + '</td>' +
                        '<td>' + data.data[i].RANKED + '</td>' +

                        '<td>' + (data.data[i].SCREENSHOOT_1 === null ? '<i>*Tidak ada screenshoot</i>' :
                            '<a href="' + data.data[i].SCREENSHOOT_1 + '" class="btn btn-primary btn-action" target="_blank"><i class="fas fa-images"></i> Lihat Screenshoot</a>') +
                        '</td>' +

                        '<td>' + (data.data[i].SCREENSHOOT_2 === null ? '<i>*Tidak ada screenshoot</i>' :
                            '<a href="' + data.data[i].SCREENSHOOT_2 + '" class="btn btn-primary btn-action" target="_blank"><i class="fas fa-images"></i> Lihat Screenshoot</a>') +
                        '</td>' +

                        '<td>' + (data.data[i].SCREENSHOOT_3 === null ? '<i>*Tidak ada screenshoot</i>' :
                            '<a href="' + data.data[i].SCREENSHOOT_3 + '" class="btn btn-primary btn-action" target="_blank"><i class="fas fa-images"></i> Lihat Screenshoot</a>') +
                        '</td>' +

                        '</tr>'
                }

                $('#tbody-modal').html(tbody);
                $('#modalViewIDGame').modal('show');
            }
        });
    });

    $(document).on("click", ".btn-view-location", function(e) {
        const id = $(this).data('id');
        const url = "<?= base_url() ?>/api/locations/" + id;
        $.get(url, function(data, status) {
            console.log(status);
            if (status === "success") {
                console.log(data);
                window.open("https://www.google.com/maps/search/?api=1&query=" + data.data.LATITUDE + "," + data.data.LONGITUDE, "_blank");
            }
        });
    });

    <?php if (session()->getFlashData('pesan')) : ?>
        swal('Sukses', '<?= session()->getFlashData('pesan'); ?>', 'success', {
            buttons: false,
            timer: 1200,
        });
    <?php endif ?>
</script>
<?= $this->endSection() ?>